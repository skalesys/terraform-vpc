#------
# MAIN
#------

variable "namespace" {
  type        = "string"
  default     = "sk"
  description = "Organization namespace"
}

variable "stage" {
  type        = "string"
  description = "Stage (short name), e.g. 'prd', 'stg', 'tst', 'dev'"
}

variable "environment" {
  description = "Stage (long name), e.g. 'production', 'staging', 'testing', 'development "
}

variable "name" {
  type        = "string"
  default     = "skalesys"
  description = "Solution name"
}

variable "delimiter" {
  type        = "string"
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage`, etc."
}

variable "tags" {
  type = "map"

  default = {
    "Owner      " = "SkaleSys"
    "Office     " = "Perth"
  }

  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}

variable "region" {
  type        = "string"
  default     = "ap-southeast-2"
  description = "AWS Region"
}

#-----
# VPC
#-----

variable "availability_zones" {
  type        = "list"
  default     = ["ap-southeast-2a", "ap-southeast-2b", "ap-southeast-2c", "ap-southeast-2d"]
  description = "Availability Zones"
}

variable "private_subnets" {
  type        = "list"
  description = "A list of private subnets inside the VPC"
}

variable "public_subnets" {
  type        = "list"
  description = "A list of public subnets inside the VPC"
}

variable "intra_subnets" {
  type        = "list"
  description = "A list of intra subnets"
}

variable "database_subnets" {
  type        = "list"
  description = "A list of database subnets"
}

variable "elasticache_subnets" {
  type        = "list"
  description = "A list of elasticache subnets"
}

variable "assign_generated_ipv6_cidr_block" {
  description = "Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC. You cannot specify the range of IP addresses, or the size of the CIDR block"
}

variable "enable_nat_gateway" {
  description = "Should be true if you want to provision NAT Gateways for each of your private networks	"
}

variable "single_nat_gateway" {
  description = "Should be true if you want to provision a single shared NAT Gateway across all of your private networks	"
}

variable "enable_dns_hostnames" {
  description = "Whether or not the VPC has DNS hostname support"
}

variable "one_nat_gateway_per_az" {
  description = "Should be true if you want only one NAT Gateway per availability zone. Requires var.azs to be set, and the number of public_subnets created to be greater than or equal to the number of availability zones specified in var.azs."
}

variable "enable_dns_support" {
  description = "Should be true to enable DNS support in the VPC"
}

variable "cidr" {
  description = "The CIDR block of the VPC"
}

variable "enable_vpn_gateway" {
  description = "Should be true if you want to create a new VPN Gateway resource and attach it to the VPC"
}

#---------------
# DIRECT CONNECT
#---------------

variable "connection_name" {
  default = "tf-dx-connection"
}

variable "connection_bandwith" {
  description = "DX Connection Bandwidth 1Gbps or 10Gbps"
  default     = "1Gbps"
}

variable "connection_location" {
  description = "AWS Direct connect location"
  default     = "NDCP1"
}
