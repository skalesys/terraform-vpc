

![logo][logo]





 
# terraform-vpc 


 
Terraform workspace that provision AWS VPC with a private, public, intra, database and elasticache subnets. 


---


## Screenshots


![vpc-description](docs/screenshots/vpc-description.png) |
|:--:|
| *VPC description* |


![vpc-cidr-blocks](docs/screenshots/vpc-cidr-blocks.png) |
|:--:|
| *VPC CIDR blocks* |


![vpc-tags](docs/screenshots/vpc-tags.png) |
|:--:|
| *VPC tags* |


![subnets](docs/screenshots/subnets.png) |
|:--:|
| *Subnets* |


![intra-subnet-description](docs/screenshots/intra-subnet-description.png) |
|:--:|
| *Intra subnet description* |


![intra-subnet-rt](docs/screenshots/intra-subnet-rt.png) |
|:--:|
| *Intra subnet route table* |


![private-subnet-description](docs/screenshots/private-subnet-description.png) |
|:--:|
| *Private subnet description* |


![private-subnet-route-table](docs/screenshots/private-subnet-rt.png) |
|:--:|
| *Private subnet route table* |


![public-subnet-description](docs/screenshots/public-subnet-description.png) |
|:--:|
| *Public subnet description* |


![public-subnet-route-table](docs/screenshots/public-subnet-rt.png) |
|:--:|
| *Public subnet route table* |


![subnet-db-description](docs/screenshots/subnet-db-description.png) |
|:--:|
| *Subnet db description* |


![subnet-db-route-table](docs/screenshots/subnet-db-rt.png) |
|:--:|
| *Subnet db route table* |


![subnet-elasticache-description](docs/screenshots/subnet-elasticache-description.png) |
|:--:|
| *Subnet elasticache description* |


![subnet-elasticache-route-table](docs/screenshots/subnet-elasticache-rt.png) |
|:--:|
| *Subnet elasticache route table* |


![direct-connect-connections](docs/screenshots/direct-connect-connections.png) |
|:--:|
| *Direct connect connections* |


![direct-connect-connection-vifs](docs/screenshots/direct-connect-connection-vifs.png) |
|:--:|
| *Direct connect VIFs* |


![direct-connect-vifs](docs/screenshots/direct-connect-vifs.png) |
|:--:|
| *Direct connect VIFs* |


![direct-connect-vif-peering](docs/screenshots/direct-connect-vif-peering.png) |
|:--:|
| *Direct connect VIF peering* |


![direct-connect-vif-virtual-interfaces](docs/screenshots/direct-connect-vif-virtual-interfaces.png) |
|:--:|
| *Direct connect VIF virtual interfaces* |


![direct-connect-vgws](docs/screenshots/direct-connect-vgws.png) |
|:--:|
| *Direct connect VGW (virtual private gateway* |









## Requirements

In order to run this project you will need: 

- [Ubuntu 18.04][ubuntu] - Linux operating system
- [Terraform][terraform] - Write, Plan, and Create Infrastructure as Code




## Usage

```bash
make init install
make terraform/plan STAGE=development
make terraform/apply STAGE=development
```
Outputs:
```
account_id = 096373988534
arn = arn:aws:iam::096373988534:user/vhenrique
azs = [
    ap-southeast-2a,
    ap-southeast-2b,
    ap-southeast-2c,
    ap-southeast-2d
]
database_network_acl_id =
database_route_table_ids = [
    rtb-0af2dbedb1a63cc09,
    rtb-090b1d304bbc26f39,
    rtb-014f572a0ce8d1703
]
database_subnet_group = sk-dev-skalesys-vpc
database_subnets = [
    subnet-09fc5f2ecbde4e90b,
    subnet-04ccff95032e6aec9
]
database_subnets_cidr_blocks = [
    10.1.21.0/24,
    10.1.22.0/24
]
elasticache_network_acl_id =
elasticache_route_table_ids = [
    rtb-0af2dbedb1a63cc09,
    rtb-090b1d304bbc26f39,
    rtb-014f572a0ce8d1703
]
elasticache_subnet_group = sk-dev-skalesys-vpc
elasticache_subnet_group_name = sk-dev-skalesys-vpc
elasticache_subnets = [
    subnet-0c8fdf92d44849a81,
    subnet-0943cd5ff6192ce29
]
elasticache_subnets_cidr_blocks = [
    10.1.31.0/24,
    10.1.32.0/24
]
igw_id = igw-053c7075ef5f3dceb
intra_network_acl_id =
intra_route_table_ids = [
    rtb-02cf36b2a7c6a860b
]
intra_subnets = [
    subnet-05402d1f575ba3302,
    subnet-092a2c73680407153
]
intra_subnets_cidr_blocks = [
    10.1.41.0/24,
    10.1.42.0/24
]
nat_ids = [
    eipalloc-0399d8a0ed60768ce,
    eipalloc-0685028b247d1fcdc,
    eipalloc-05df55a30d5c54022
]
nat_public_ips = [
    13.55.217.128,
    3.105.56.93,
    54.79.21.144
]
natgw_ids = [
    nat-0ca0678075e1fb5bc,
    nat-01f71dafdea377fc5,
    nat-06beccc85e1aae8bc
]
private_network_acl_id =
private_route_table_ids = [
    rtb-0af2dbedb1a63cc09,
    rtb-090b1d304bbc26f39,
    rtb-014f572a0ce8d1703
]
private_subnets = [
    subnet-0f609a98ba14544f3,
    subnet-0d5bbf3d34a49162e,
    subnet-0b48716cd86be8fc8
]
private_subnets_cidr_blocks = [
    10.1.1.0/24,
    10.1.2.0/24,
    10.1.3.0/24
]
public_network_acl_id =
public_route_table_ids = [
    rtb-0581e16404fe8c3bb
]
public_subnets = [
    subnet-0b11efeaeee457eaf,
    subnet-0b9ae04e29a796a98,
    subnet-04e3f83b777e7cfc9
]
public_subnets_cidr_blocks = [
    10.1.11.0/24,
    10.1.12.0/24,
    10.1.13.0/24
]
region = ap-southeast-2
user_id = AIDAIOLCLPNKFG2456DOW
vpc_cidr_block = 10.1.0.0/16
vpc_enable_dns_hostnames = true
vpc_enable_dns_support = true
vpc_id = vpc-048f171535a31f361
vpc_instance_tenancy = default
vpc_label = {
  attributes = [vpc]
  id = sk-dev-skalesys-vpc
  name = skalesys
  namespace = sk
  stage = dev
}
vpc_label_context = {
  attributes = [vpc]
  delimiter = [-]
  environment = []
  label_order = [namespace environment stage name attributes]
  name = [skalesys]
  namespace = [sk]
  regex_replace_chars = [/[^a-zA-Z0-9-]/]
  stage = [dev]
  tags_keys = [Environment Name Namespace Office      Owner       Stage]
  tags_values = [development sk-dev-skalesys-vpc sk Perth SkaleSys dev]
}
vpc_label_tags = {
  Environment = development
  Name = sk-dev-skalesys-vpc
  Namespace = sk
  Office      = Perth
  Owner       = SkaleSys
  Stage = dev
}
vpc_main_route_table_id = rtb-05db366d733ac5fe1
```







## Makefile targets

```Available targets:

  aws-nuke/install                   	Install aws-nuke
  base                               	Runs base playbook
  clean                              	Clean roots
  docker/install                     	Install docker
  gomplate/install                   	Install gomplate
  google-chrome/install              	Install google-chrome
  help/all                           	Display help for all targets
  help/all/plain                     	Display help for all targets
  help                               	Help screen
  help/short                         	This help short screen
  install                            	Install project requirements
  java/install                       	Install java
  openvpn/install                    	Install openvpn
  packer/install                     	Install packer
  packer/version                     	Prints the packer version
  pip/install                        	Install pip
  readme                             	Alias for readme/build
  readme/build                       	Create README.md by building it from README.yaml
  readme/install                     	Install README
  security/install                   	Install security
  spotify/install                    	Install spotify
  terraform/apply                    	Builds or changes infrastructure
  terraform/clean                    	Cleans Terraform vendor from Maker
  terraform/console                  	Interactive console for Terraform interpolations
  terraform/destroy                  	Destroy Terraform-managed infrastructure, removes .terraform and local state files
  terraform/fmt                      	Rewrites config files to canonical format
  terraform/get                      	Download and install modules for the configuration
  terraform/graph                    	Create a visual graph of Terraform resources
  terraform/init-backend             	Initialize a Terraform working directory with S3 as backend and DynamoDB for locking
  terraform/init                     	Initialize a Terraform working directory
  terraform/install                  	Install terraform
  terraform/output                   	Read an output from a state file
  terraform/plan                     	Generate and show an execution plan
  terraform/providers                	Prints a tree of the providers used in the configuration
  terraform/push                     	Upload this Terraform module to Atlas to run
  terraform/refresh                  	Update local state file against real resources
  terraform/show                     	Inspect Terraform state or plan
  terraform/taint                    	Manually mark a resource for recreation
  terraform/untaint                  	Manually unmark a resource as tainted
  terraform/validate                 	Validates the Terraform files
  terraform/version                  	Prints the Terraform version
  terraform/workspace                	Select workspace
  update                             	Updates roots
  vagrant/destroy                    	Stops and deletes all traces of the vagrant machine
  vagrant/install                    	Install vagrant
  vagrant/recreate                   	Destroy and creates the vagrant environment
  vagrant/update/boxes               	Updates all Vagrant boxes
  vagrant/up                         	Starts and provisions the vagrant environment
  version                            	Displays versions of many vendors installed
  virtualbox/install                 	Install virtualbox
  vlc/install                        	Install vlc
```








## References

For additional context, refer to some of these links. 

- [AWS VPC](https://aws.amazon.com/vpc/) - Amazon Virtual Private Cloud (Amazon VPC) lets you provision a logically isolated section of the AWS Cloud where you can launch AWS resources in a virtual network that you define.
- [AWS Direct Connect](https://aws.amazon.com/directconnect/) - Direct Connect is a service for establishing a dedicated network connection from your premise to AWS at a colocation facility.




## Resources

Resources used to create this project: 

- [Photo](https://unsplash.com/photos/h3vT1Kp0FxA) - Photo by Thomas Jensen on Unsplash
- [Gitignore.io](https://gitignore.io) - Defining the `.gitignore`
- [LunaPic](https://www341.lunapic.com/editor/) - Image editor (used to create the avatar)





## Repository

We use [SemVer](http://semver.org/) for versioning. 

- **[Branches][branches]**
- **[Commits][commits]**
- **[Tags][tags]**
- **[Contributors][contributors]**
- **[Graph][graph]**
- **[Charts][charts]**









## Contributors

Thank you so much for making this project possible: 

- [Valter Silva](https://gitlab.com/valter-silva)



## Copyright

Copyright © 2019-2019 [SkaleSys][company]





[logo]: docs/logo.jpeg


[company]: https://skalesys.com
[contact]: https://skalesys.com/contact
[services]: https://skalesys.com/services
[industries]: https://skalesys.com/industries
[training]: https://skalesys.com/training
[insights]: https://skalesys.com/insights
[about]: https://skalesys.com/about
[join]: https://skalesys.com/Join-Our-Team

[ansible]: https://ansible.com
[terraform]: http://terraform.io
[packer]: https://www.packer.io
[docker]: https://www.docker.com/
[vagrant]: https://www.vagrantup.com/
[kubernetes]: https://kubernetes.io/
[spinnaker]: https://www.spinnaker.io/
[jenkins]: https://jenkins.io/
[aws]: https://aws.amazon.com/
[ubuntu]: https://ubuntu.com/




[aws]: https://aws.amazon.com/
[ubuntu]: https://ubuntu.com/






[branches]: https://gitlab.com/skalesys/terraform-vpc/branches
[commits]: https://gitlab.com/skalesys/terraform-vpc/commits
[tags]: https://gitlab.com/skalesys/terraform-vpc/tags
[contributors]: https://gitlab.com/skalesys/terraform-vpc/graphs
[graph]: https://gitlab.com/skalesys/terraform-vpc/network
[charts]: https://gitlab.com/skalesys/terraform-vpc/charts


